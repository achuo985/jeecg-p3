		<#if jpTeamPerson.createName ?exists && jpTeamPerson.createName ?length gt 0>
		    /* 创建人名称 */
			and jtp.create_name like CONCAT('%', :jpTeamPerson.createName ,'%') 
		</#if>
		<#if jpTeamPerson.createBy ?exists && jpTeamPerson.createBy ?length gt 0>
		    /* 创建人登录名称 */
			and jtp.create_by like CONCAT('%', :jpTeamPerson.createBy ,'%') 
		</#if>
	    <#if jpTeamPerson.createDate ?exists>
		    /* 创建日期 */
			and jtp.create_date = :jpTeamPerson.createDate
		</#if>
		<#if jpTeamPerson.updateName ?exists && jpTeamPerson.updateName ?length gt 0>
		    /* 更新人名称 */
			and jtp.update_name like CONCAT('%', :jpTeamPerson.updateName ,'%') 
		</#if>
		<#if jpTeamPerson.updateBy ?exists && jpTeamPerson.updateBy ?length gt 0>
		    /* 更新人登录名称 */
			and jtp.update_by like CONCAT('%', :jpTeamPerson.updateBy ,'%') 
		</#if>
	    <#if jpTeamPerson.updateDate ?exists>
		    /* 更新日期 */
			and jtp.update_date = :jpTeamPerson.updateDate
		</#if>
		<#if jpTeamPerson.sysOrgCode ?exists && jpTeamPerson.sysOrgCode ?length gt 0>
		    /* 所属部门 */
			and jtp.sys_org_code like CONCAT('%', :jpTeamPerson.sysOrgCode ,'%') 
		</#if>
		<#if jpTeamPerson.sysCompanyCode ?exists && jpTeamPerson.sysCompanyCode ?length gt 0>
		    /* 所属公司 */
			and jtp.sys_company_code like CONCAT('%', :jpTeamPerson.sysCompanyCode ,'%') 
		</#if>
		<#if jpTeamPerson.name ?exists && jpTeamPerson.name ?length gt 0>
		    /* 名称 */
			and jtp.name like CONCAT('%', :jpTeamPerson.name ,'%') 
		</#if>
		<#if jpTeamPerson.imgSrc ?exists && jpTeamPerson.imgSrc ?length gt 0>
		    /* 头像路径 */
			and jtp.img_src like CONCAT('%', :jpTeamPerson.imgSrc ,'%') 
		</#if>
		<#if jpTeamPerson.introduction ?exists && jpTeamPerson.introduction ?length gt 0>
		    /* 简介 */
			and jtp.introduction like CONCAT('%', :jpTeamPerson.introduction ,'%') 
		</#if>
	    <#if jpTeamPerson.jionDate ?exists>
		    /* 加入时间 */
			and jtp.jion_date = :jpTeamPerson.jionDate
		</#if>
		<#if jpTeamPerson.isJoin ?exists && jpTeamPerson.isJoin ?length gt 0>
		    /* 是否参与；1为是，0为否 */
			and jtp.is_join like CONCAT('%', :jpTeamPerson.isJoin ,'%') 
		</#if>
